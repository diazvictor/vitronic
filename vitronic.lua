#!/usr/bin/env lua

--[[--
 @package   Vitronic
 @filename  init.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      26.05.2020 17:05:53 -04
]]

VERSION		= 0.5
package.path  	= package.path .. ";libraries/?.lua;vendor/?.lua";
lgi 		= require('lgi')
Gtk 		= lgi.require('Gtk', '3.0')
Gdk 		= lgi.Gdk
GLib 		= lgi.GLib
assert  	= lgi.assert
builder 	= Gtk.Builder({
	translation_domain = 'vitronic'
})
require 	('libraries.luachi')
assert(
		builder:add_from_file('views/vitronic.ui'),
		gettext('ERROR: view not found')
)
ui 		= builder.objects

-- La carga de los estilos css
local provider = Gtk.CssProvider()
provider:load_from_path('views/css/vitronic.css')
local screen = Gdk.Display.get_default_screen(Gdk.Display:get_default())
local GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600
Gtk.StyleContext.add_provider_for_screen(
	screen, provider,
	GTK_STYLE_PROVIDER_PRIORITY_APPLICATION
)

---Funcion para cerrar el programa
function quit()
	db:close()
	Gtk.main_quit()
end

---Al hacer click en el boton de cancelar del calendario
function ui.calendar_cancel:on_clicked()
	ui.calendar_window:hide()
end

---La version en el about
ui.about_window.version = VERSION

---Al hacer click en el about
function ui.menu_about:on_clicked()
	ui.about_window:run()
	ui.about_window:hide()
end

---Al hacer click en el menu de configuracion
function ui.menu_setting:on_clicked()
	ui.setting_window:run()
	ui.setting_window:hide()
end

---Al hacer click en el menu quit
function ui.menu_quit:on_clicked()
	quit()
end

---Al hacer click en el boton salir
function ui.sidebar_quit:on_clicked()
	quit()
end

---Al cerrar la ventana
function ui.main_window:on_destroy()
	quit()
end

ui.main_window:set_auto_startup_notification(false)
if ( db:get_var('select name from company') ) then
	require('components.splash')
	require('components.common')
	require('components.clients')
	require('components.entries')
	require('components.clock')
	require('components.settings')
	require('components.statusicon')
else
	require('components.assistant')
end
Gtk.main()
