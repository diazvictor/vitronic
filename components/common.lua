--[[--
 @package   Vitronic
 @filename  common.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      27.05.2020 23:05:53 -04
]]


--Espero n segundos y borro el mensaje en el statusbar
function clean_status_bar(seconds)
	seconds = seconds or 3
	GLib.timeout_add_seconds(
	    GLib.PRIORITY_DEFAULT, seconds,
	    function()
		ui.statusbar:push(0, '')
		return false
	    end
	)
end

--- muestra el calendario y setea su valor en el elemento
-- recibido en el argumento
function calendar(elelemt)
	function ui.calendar_accept:on_clicked()
		local y, m, d = ui.calendar:get_date()
		m = tonumber(m)+1
		d = tonumber(d)+1
		if ( string.len(m) == 1 ) then
			m = ('0%s'):format(m)
		elseif ( string.len(d) == 1 ) then
			d = ('0%s'):format(d)
		end

		local value = ('%s-%s-%s'):format(y, m, d)
		elelemt.text = value
		ui.calendar_window:hide()
	end
	ui.calendar_window:run()
	ui.calendar_window:hide()
end

--- funcion para mostrar un dialogo INFO
function dialog(msg, transientfor)
	local dialog = Gtk.MessageDialog {
		transient_for = transientfor,
		title 	= 'Mensaje',
		modal = true,
		destroy_with_parent = true,
		message_type = 'INFO',
		buttons = 'OK',
		text = msg
	}
	dialog:set_decorated(false)
	dialog:run()
	dialog:destroy()
end

--- Rutina para habilitar fullscreen con F11
local fullscreen = false
function ui.main_window:on_key_release_event(env)
    if ( env.keyval  == Gdk.KEY_F11 ) then
		fullscreen = not fullscreen
		if ( fullscreen ) then
			ui.main_window:fullscreen()
		else
			ui.main_window:unfullscreen()
		end
    end
end