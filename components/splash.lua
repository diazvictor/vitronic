--[[
 @package   Vitronic
 @filename  splash.lua
 @version   1.0
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      05.06.2020 14:05:37 -04
]]--



local progressbar = ui.splash_progress
GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100,function()
	local fraction = progressbar.fraction + 0.10
	if (fraction < 1) then
		progressbar.fraction = fraction
		return true
	else
		--destruyo el splash
		ui.splash:destroy()
		--Seteo la ventana a su tamaño maximo
		ui.main_window:maximize()
		--Muestro la ventana principal
		ui.main_window:show_all()
		--seteo el statusicon segun la configuracion
		statusicon:set_visible(util:is_true(settings['trayicon']))
		--Notifico que la aplicacion finalmente inicio
		ui.main_window:set_auto_startup_notification(true)
		--Finalizo el bucle del timeout
		return false
	end
end)

--Mantengo el splash siempre arriba
ui.splash:set_keep_above(true)
ui.splash:show_all()
