--[[
 @package   Vitronic
 @filename  clients.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      26.05.2020 20:33:43 -04
]]--

local id_client = nil

--- Retorna toda la lista de clientes
function get_clients(search)
	-- if util:trim(search) == '' then return {} end
	local sql, results
	if ( search ) then
		sql = [[select *
				from clients
			where fullname like %s order by id_client desc]]
		results = db:get_results(sql, "%"..util:trim(search).."%")
	else
		sql = "select * from clients order by id_client desc"
		results = db:get_results(sql)
	end
	return results
end

--- Funcion que Puebla la vista de clientes
function populate_client_view(search)
	local results = get_clients(search)

	for i, item in ipairs(results) do
		ui.client_view:append({
			item.id_client,
			item.fullname,
			item.phone,
			item.address,
			item.created_at
		})
	end
end

--Limpio el treeview y lo pueblo
ui.client_view:clear()
populate_client_view()

--- Limpia el formulario de clientes
function clean_client_form()
	ui.client_fullname.text	   = ''
	ui.client_pasport.text	   = ''
	ui.client_phone.text	   = ''
	ui.client_phone2.text	   = ''
	ui.client_address.text	   = ''
	ui.client_observation.text = ''
end

--- Al hacer click en el boton de "Imprimir"
function ui.client_print:on_clicked()
	local consulta = get_clients()
	for i, item in ipairs(consulta) do
		print(item.id_client,
			item.fullname,
			item.phone,
			item.address,
			item.created_at)
	end
end

--- Retorna todos los datos de un cliente
function get_client()
	local sql = 'select * from clients where id_client = %s'
	local results = db:get_rows(sql, id_client)
	return results[1]
end

--- Puebla el formulario de clientes
function populate_client_form()
	local data = get_client()

	ui.client_fullname.text = data.fullname
	ui.client_pasport.text = data.passport_card
	ui.client_phone.text = data.phone
	ui.client_phone2.text = data.other_phone
	ui.client_address.text = data.address
	ui.client_observation.text = data.observation
end

---Funcion de sanidad al guardar un registro de cliente
function sanity_save_client()
	local msg

	--Verifica si un C.I/R.I.F existe
	function pasport_exist(pasport)
		local sql = [[select id_client
				from clients
			      where passport_card = %s]]
		if (id_client) then
			sql = sql .. ' and id_client != %d'
			return db:get_var(sql, util:trim(pasport), id_client )
		end
		return db:get_var(sql, util:trim(pasport) )
	end

	--Verifica si un telefono existe
	function phone_exist(phone)
		local sql = [[select id_client
				from clients
			      where phone = %s]]
		if (id_client) then
			sql = sql .. ' and id_client != %d'
			return db:get_var(sql, util:trim(phone), id_client )
		end
		return db:get_var(sql, util:trim(phone) )
	end

	--Las validaciones
	if     (util:trim(ui.client_fullname.text) == '') then
		msg = 'El nombre del cliente es un campo obligatorio.'
		ui.client_fullname:grab_focus()
	elseif (util:trim(ui.client_pasport.text)  == '') then
		msg = 'El C.I/R.I.F del cliente es un campo obligatorio.'
		ui.client_pasport:grab_focus()
	elseif ( pasport_exist(ui.client_pasport.text) ) then
		msg = 'El C.I/R.I.F ya esta registrado.'
		ui.client_pasport:grab_focus()
	elseif (util:trim(ui.client_phone.text)    == '') then
		msg = 'El telefono del cliente es un campo obligatorio.'
		ui.client_phone:grab_focus()
	elseif (phone_exist(ui.client_phone.text)) then
		msg = 'El telefono ya esta registrado.'
		ui.client_phone:grab_focus()
	elseif (util:trim(ui.client_address.text)  == '') then
		msg = 'La dirección del cliente es un campo obligatorio.'
		ui.client_address:grab_focus()
	end

	if ( msg ) then
		return false, msg
	end
	return true
end

--- Funcion para guardar los datos del formulario de clientes
function save_client()
	local values = {
		ui.client_fullname.text,
		ui.client_phone.text,
		ui.client_phone2.text,
		ui.client_address.text,
		ui.client_observation.text,
		ui.client_pasport.text
	}
	local sql, msg

	local sanity, err = sanity_save_client()
	if ( not sanity ) then
		return false, err
	end

	if (id_client) then
		sql = [[
			update clients set fullname = %s, phone = %s,
			other_phone = %s,address = %s,
			observation = %s, passport_card = %s
			where id_client = %d
		]]
		--Inserto el id_cliente al final de la tabla values
		table.insert(values, id_client)
		msg = 'Cliente actualizado con exito!'
	else
		sql = [[
			insert into clients(
				fullname, phone, other_phone,
				address, observation, passport_card
			)
			values (%s, %s, %s, %s, %s, %s)
		]]
		msg = 'Cliente registrado con exito!'
	end

	local ok, err = db:execute(sql, values)
	if (not ok) then
		return false, err
	end
	id_client = nil
	populate_entries_client()
	return true, msg
end

--- Funcion que borra un cliente
function delete_client(id_client)
	local sql = 'delete from clients where id_client = %d'
	local ok, err = db:execute(sql, id_client)
	if (not ok) then
		return false, err
	end
	id_client = nil
	ui.statusbar:push(0, 'Cliente removido con exito!')
	--Espero 2 segundos y borro el mensaje en el statusbar
	clean_status_bar(2)
	return true
end

-- Al hacer click en el boton de guardar del formulario de clientes
function ui.client_save_form:on_clicked()
	local ok, msg = save_client()
	if ok then
		ui.client_expander:set_expanded(false)
		ui.client_view:clear()
		populate_client_view()
		clean_client_form()
		ui.statusbar:push(0, msg)

		--Espero 2 segundos y borro el mensaje en el statusbar
		clean_status_bar(2)
		return
	end
	ui.statusbar:push(0, ('ERROR : %s'):format(msg) )
	--Espero 2 segundos y borro el mensaje en el statusbar
	clean_status_bar(2)
end

--- Al presionar el boton de borrar cliente
function ui.client_delete:on_clicked()
	local id = get_client_row_id()
	if ( not id ) then
		dialog(
			'Seleccione un cliente de la lista',
			ui.main_window
		)
	else
		delete_client(id)
		ui.client_view:clear()
		populate_client_view()
		clean_client_form()
	end
end

--- Setea la variable id_client y tambien la retorna, toma el dato
-- de la fila activa en el treeview de clientes 
function get_client_row_id()
	local selection = ui.client_treeview:get_selection()
	selection.mode = 'SINGLE'
	local model, iter = selection:get_selected()
	if model and iter then
		id_client = model:get_value(iter, 0):get_int()
		-- local fullname = model:get_value(iter, 1):get_string()

		return id_client
	end
end

--- Reordenamiento de la columna ID
function ui.id_view_client:on_clicked()
	self:set_sort_column_id(0)
end

--- Al hacer doble click en una fila del treeview de clientes
function ui.client_treeview:on_row_activated()
	get_client_row_id()
	populate_client_form()
	ui.client_expander:set_expanded(true)
end

--- Al hacer click en el boton de "Nuevo Cliente"
function ui.client_new:on_clicked()
	go_stack_form_client()
	id_client = nil
end

--- Al hacer click en el boton de cancelar del formulario de clientes
function ui.client_cancel_form:on_clicked()
	clean_client_form()
	ui.client_expander:set_expanded(false)
end

--- Ir al formulario de registro de cliente
function go_stack_form_client()
	ui.stack1:set_visible_child(ui.stack_clients)
	ui.client_expander:set_expanded(true)
	clean_client_form()
	ui.client_fullname:grab_focus()
end

--- Al escribir algo en el buscador de clientes
function ui.client_search:on_changed()
	local search = ui.client_search.text
	ui.client_view:clear()
	populate_client_view(search)
end