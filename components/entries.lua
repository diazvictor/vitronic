--[[
 @package   Vitronic
 @filename  entries.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      26.05.2020 22:59:30 -04
]]--

local id_entrie = nil

--- Retorna toda la lista de ingresos
function get_entries(search)
	local results
	local order = ' order by id_entrie desc '
	local sql = [[select
		      entries.code, entries.id_entrie,
		      clients.fullname, entries.device,
		      entries.serial, vendors.vendor,
		      entries.problem, entries.quoted,
		      entries.delivery
				from entries
		      inner join vendors on (
				vendors.id_vendor = entries.id_vendor
		      )
		      inner join clients on (
				clients.id_client = entries.id_client
		      )
	]]
	if (search) then
		sql = sql .. ' where clients.fullname like %s ' .. order
		results = db:get_results(sql, "%"..util:trim(search).."%")
	else
		results = db:get_results(sql .. order)
	end
	return results
end

function get_advance(id_entrie)
	local sql = [[
		select sum(amount)
			from advances
		where id_entrie = %d
	]]
	local result = db:get_var(sql, id_entrie)
	if not result then
		return '0'
	end
	return result
end

--- Funcion que Puebla la vista de ingresos
function populate_entries_view(search)
	local results = get_entries(search)
	for i, item in ipairs(results) do
		if ( not item.serial or item.serial == '' ) then
			item.serial = 'S/S'
		end

		if ( not item.quoted or item.quoted == '' ) then
			item.quoted = 'N/C'
		else
			item.quoted = util:number2money(('%.2f'):format(item.quoted))
		end

		ui.entries_view:append({
			item.id_entrie,
			item.code,
			item.fullname,
			item.device,
			item.serial,
			item.vendor,
			item.problem,
			item.quoted,
			util:number2money(('%.2f'):format(get_advance(item.id_entrie))),
			item.delivery
		})
	end
end

---Limpio el treeview y lo pueblo
ui.entries_view:clear()
populate_entries_view()

--- Puebla el selector de clientes
function populate_entries_client()
	ui.entrie_id_client:remove_all()
	local sql = 'select * from clients order by id_client desc'
	local results = db:get_results(sql)
	for i, item in ipairs(results) do
		ui.entrie_id_client:append(
			item.id_client,
			item.fullname
		)
	end
end
populate_entries_client()

--- Puebla el selector de marcas
function populate_entries_vendor()
	ui.entrie_id_vendor:remove_all()
	local sql = 'select * from vendors order by id_vendor desc'
	local results = db:get_results(sql)
	for i, item in ipairs(results) do
		ui.entrie_id_vendor:append(
			item.id_vendor,
			item.vendor
		)
	end
end
populate_entries_vendor()

-- Al hacer click en "Imprimir"
function ui.entrie_print:on_clicked()
	local consulta = get_entries()
	for i, item in ipairs(consulta) do
		print(item.fullname,
			item.device,
			item.serial,
			item.vendor,
			item.problem,
			item.delivery)
	end
end

-- Borrar ingreso
function delete_entrie()
	if not id_entrie then
		dialog(
			'Seleccione un ingreso de la lista',
			ui.main_window
		)
	else
		local sql = [[
			delete from advances where id_entrie = %d;
			delete from entries where id_entrie = %d;
		]]
		local ok, err = db:execute(sql, id_entrie,id_entrie)
		if (not ok) then
			return false, err
		end
		id_entrie = nil
		ui.statusbar:push(0, 'Ingreso removido con exito!')
		ui.entries_view:clear()
		populate_entries_view()
		clean_entrie_form()
		--Espero 2 segundos y borro el mensaje en el statusbar
		clean_status_bar(2)
		return true
	end
	return false
end

-- Al hacer click en "Borrar Ingreso"
function ui.entrie_delete:on_clicked()
	get_entrie_row_id()
	if ( not id_entrie ) then
		--mostrar mensaje aqui
		dialog(
			'Seleccione un elemento para borrar',
			ui.main_window
		)
	else
		delete_entrie()
	end
end

---Funcion de sanidad al guardar un registro de cliente
function sanity_save_entrie()
	local msg

	--Las validaciones
	if ( util:trim(ui.entrie_quoted.text) == '' ) then
		ui.entrie_quoted.text = '0.0'
	else
		if ( not tonumber(ui.entrie_quoted.text) ) then
			msg = 'El monto debe ser numerico.'
			ui.entrie_quoted:grab_focus()
		end
	end
	if ( not ui.entrie_id_client:get_active_id() ) then
		msg = 'Seleccione un cliente.'
		ui.entrie_id_client:grab_focus()
	elseif (util:trim(ui.entrie_equip.text)    == '') then
		msg = 'Ingrese el equipo.'
		ui.entrie_equip:grab_focus()
	elseif (not ui.entrie_id_vendor:get_active_id()) then
		msg = 'Seleccione la marca del equipo.'
		ui.entrie_id_vendor:grab_focus()
	elseif (util:trim(ui.entrie_problem.text)  == '') then
		msg = 'Describa el problema del equipo.'
		ui.entrie_problem:grab_focus()
	elseif (util:trim(ui.entrie_delivery.text)  == '') then
		msg = 'Ingrese la fecha de entrega.'
		--ui.entrie_delivery:grab_focus()
	end

	if ( msg ) then
		return false, msg
	end
	return true
end

--- Funcion para guardar los datos del formulario de ingresos
function save_entrie()

	local sanity, err = sanity_save_entrie()
	if ( not sanity ) then
		return false, err
	end

	local values = {
		ui.entrie_id_client:get_active_id(),
		ui.entrie_id_vendor:get_active_id(),
		ui.entrie_equip.text,
		ui.entrie_serial.text,
		ui.entrie_problem.text,
		ui.entrie_quoted.text or '0',
		ui.entrie_delivery.text,
		ui.entrie_observation.text or ''
	}
	local sql, msg, code

	--- Genera el codigo de ingreso
	function gen_code()
		local sql = 'select max(id_entrie) from entries'
		local code = db:get_var(sql)
		if not code then code = 1 end
		return ('%06s'):format(tonumber(code)+1):gsub(" ", "0")
	end

	if (id_entrie) then
		sql = [[update entries set
			id_client = %d, id_vendor = %d, device = %s,
			serial = %s, problem = %s, quoted = %s,
			delivery = %s, observation = %s
			where id_entrie = %d]]
		--Inserto el id_cliente al final de la tabla values
		table.insert(values, id_entrie)
		msg = 'Ingreso actualizado con exito!'
	else
		sql = [[insert into entries(
				id_client, id_vendor,
				device, serial, problem,
				quoted, delivery,
				observation, code
			)
			values (%d, %d, %s, %s, %s, %s, %s, %s, %s)]]
		code = gen_code()
		table.insert(values, code)
		msg = 'Ingreso registrado con exito!'
	end

	local ok, err = db:execute(sql, values)
	if (not ok) then
		return false, err
	end
	id_entrie = nil
	return true, msg
end

--- Al hacer click en el boton de "Nuevo Ingreso"
function ui.entrie_new:on_clicked()
	ui.entrie_expander:set_expanded(true)
	id_entrie = nil
	clean_entrie_form()
	ui.entrie_equip:grab_focus()
end

-- Al hacer click en el boton de guardar del formulario de ingresos
function ui.entrie_save_form:on_clicked()
	local ok, msg = save_entrie()
	if ok then
		ui.entrie_expander:set_expanded(false)
		ui.entries_view:clear()
		populate_entries_view()
		clean_entrie_form()
		ui.statusbar:push(0, msg)

		--Espero 2 segundos y borro el mensaje en el statusbar
		clean_status_bar(2)
		return
	end
	ui.statusbar:push(0, ('ERROR : %s'):format(msg or 'desconocido') )
	--Espero 2 segundos y borro el mensaje en el statusbar
	clean_status_bar(2)
end

--- Retorna todos los datos de un ingresos
function get_entrie()
	local sql = 'select * from entries where id_entrie = %s'
	local results = db:get_rows(sql, id_entrie)
	return results[1]
end

--- Puebla el formulario de clientes
function populate_entrie_form()
	local data = get_entrie()

	ui.entrie_id_client:set_active_id(data.id_client)
	ui.entrie_id_vendor:set_active_id(data.id_vendor)
	ui.entrie_equip.text 	   = data.device
	ui.entrie_quoted.text 	   = data.quoted
	ui.entrie_serial.text 	   = data.serial
	ui.entrie_problem.text 	   = data.problem
	ui.entrie_delivery.text    = data.delivery
	ui.entrie_observation.text = data.observation or ''
end

--- Setea la variable id_entrie y tambien la retorna, toma el dato
-- de la fila activa en el treeview de entradas
function get_entrie_row_id()
	local selection = ui.entrie_treeview:get_selection()
	selection.mode = 'SINGLE'
	local model, iter = selection:get_selected()
	if model and iter then
		id_entrie = model:get_value(iter, 0):get_int()

		return id_entrie
	end
end

---Al hacer click derecho a algun ingreso
function ui.entrie_treeview:on_button_press_event(event)
        local x = tonumber(event.x)
        local y = tonumber(event.y)
        local pthinfo = self:get_path_at_pos(x, y)
	if (pthinfo and event.type == 'BUTTON_PRESS' and event.button == 3) then
		get_entrie_row_id()
		local menu = Gtk.Menu{
			Gtk.MenuItem{
				id = "add_advance",
				label = "Avances",
				on_activate = function()
					show_form_advance()
				end
			},
			Gtk.MenuItem{
				id = "edit",
				label = "Editar",
				on_activate = function()
					edit_entry()
				end
			},
			Gtk.SeparatorMenuItem {},
			Gtk.MenuItem {
				id = "delete",
				label = "Borrar",
				on_activate = function()
					delete_entrie()
				end
			}
		}
		if id_entrie then
			menu:attach_to_widget(ui.entrie_treeview, null)
			menu:show_all()
			menu:popup(nil, nil, nil, event.button, event.time)
		end
	end
end

---Prepara todo para una edicion
function edit_entry()
	get_entrie_row_id()
	if not id_entrie then
		dialog(
		'Seleccione un ingreso de la lista',
		ui.main_window
		)
	else
		populate_entrie_form()
		ui.entrie_expander:set_expanded(true)
	end
end

--- Al hacer doble click en una fila del treeview de ingresos
function ui.entrie_treeview:on_row_activated()
	edit_entry()
end

--- Al hacer click en el boton de añadir cliente del formulario de ingresos
function ui.entrie_add_client:on_clicked()
	go_stack_form_client()
end

--- Limpia el formulario de ingresos
function clean_entrie_form()
	ui.entrie_equip.text = ''
	ui.entrie_serial.text = ''
	ui.entrie_problem.text = ''
	ui.entrie_delivery.text = ''
	ui.entrie_observation.text = ''
	ui.entrie_id_client:remove_all()
	populate_entries_client()
	ui.entrie_id_vendor:remove_all()
	populate_entries_vendor()
end

--- Al hacer click en el boton de cancelar del formulario de ingresos
function ui.entrie_cancel_form:on_clicked()
	clean_entrie_form()
	ui.entrie_expander:set_expanded(false)
end

--- Ir al formulario de registro de ingresos
function go_stack_form_entrie()
	ui.stack1:set_visible_child(ui.stack_entries)
	ui.entrie_expander:set_expanded(true)
	clean_entrie_form()
	ui.entrie_equip:grab_focus()
end

--- Al escribir algo en el buscador de entradas
function ui.entrie_search:on_changed()
	local search = ui.entrie_search.text
	ui.entries_view:clear()
	populate_entries_view(search)
end

--- Al hacer click en la fecha de entrega
function ui.entrie_delivery:on_grab_focus()
	calendar(self)
end

---muestra el formulario de avances
function show_form_advance()
	get_entrie_row_id()
	if not id_entrie then
		dialog(
			'Seleccione un ingreso de la lista',
			ui.main_window
		)
	else
		get_advances()
		ui.entrie_advance_window:run()
		ui.entrie_advance_window:hide()
	end
end

---Prepara la vista de los avances, y la puebla
function get_advances()
	--ui.entrie_advance_window:set_decorated(false)
	--ui.entrie_advance_window.title = 'Ingresar avance'
	--- Funcion que Puebla la vista de avances
	function populate_view()
		local sql = [[select *
				from advances where id_entrie = %d
			      order by id_advance desc]]
		local results = db:get_results(sql, id_entrie)
		for i, item in ipairs(results) do
			item.amount = util:number2money(('%.2f'):format(item.amount))
			ui.advance_view:append({
				item.id_advance,
				item.created_at,
				item.concept,
				item.amount
			})
		end
	end
	---Limpio el treeview y lo pueblo
	ui.advance_view:clear()
	populate_view()

	---Borro un avance
	function delete_advance()
		local sql = 'delete from advances where id_advance = %d'
		local selection = ui.advance_treeview:get_selection()
		selection.mode = 'SINGLE'
		local model, iter = selection:get_selected()
		if model and iter then
			local id = model:get_value(iter, 0):get_int()
			local ok , err = db:execute(sql, id)
			if ok then
				ui.advance_view:clear()
				populate_view()
				ui.entries_view:clear()
				populate_entries_view()
			end
		end
	end

	---Al hacer click derecho a algun ingreso
	function ui.advance_treeview:on_button_press_event(event)
		local x = tonumber(event.x)
		local y = tonumber(event.y)
		local pthinfo = self:get_path_at_pos(x, y)
		if (pthinfo and event.type == 'BUTTON_PRESS' and event.button == 3) then
			local menu = Gtk.Menu{
				Gtk.MenuItem {
					label = "Borrar",
					on_activate = function()
						delete_advance()
					end
				}
			}
			if id_entrie then
				menu:attach_to_widget(ui.advance_treeview, null)
				menu:show_all()
				menu:popup(nil, nil, nil, event.button, event.time)
			end
		end
	end
end

---Al hacer click en el boton avance o adelantos
function ui.entrie_advance:on_clicked()
	show_form_advance()
end

function save_advance()
	if (util:trim(ui.entrie_advance_amount.text) == '') then
		--dialog(
			--'Ingrese el monto del avance',
			--ui.main_window
		--)
		ui.entrie_advace_message.label = 'Ingrese el monto del avance'
		ui.entrie_advance_amount:grab_focus()
		return false
	elseif (util:trim(ui.entrie_advance_concept.text) == '') then
		--dialog(
			--'Ingrese el concepto del avance',
			--ui.main_window
		--)
		ui.entrie_advace_message.label = 'Ingrese el concepto del avance'
		ui.entrie_advance_concept:grab_focus()
		return false
	end
	local sql = [[
		insert into advances (
			id_entrie,amount,concept
		) values(
			%d, %f, %s
		)
	]]
	local values = {
		id_entrie,
		ui.entrie_advance_amount.text,
		ui.entrie_advance_concept.text	
	}
	local ok, err = db:execute(sql, values)
	if (not ok) then
		return false, err
	end
	return true
end

--- El boton cancelar avance
function ui.entrie_advance_cancel:on_clicked()
	ui.entrie_advance_window:hide()
	id_entrie = nil
end

--- El boton guardar avance
function ui.entrie_advance_save:on_clicked()
	if ( save_advance() ) then
		--ui.entrie_advance_window:hide()
		get_advances()
		ui.entrie_advance_amount.text = ''
		ui.entrie_advance_concept.text = ''
		ui.entries_view:clear()
		populate_entries_view()
	end
end

function save_vendor()

	function vendor_exist(vendor)
		local sql = 'select id_vendor from vendors where vendor = %s'
		return db:get_var(sql, vendor)
	end
	--Validaciones
	if (util:trim(ui.entrie_new_vendor.text) == '') then
		ui.entrie_vendor_message.label = 'Ingrese el nombre de la marca'
		ui.entrie_new_vendor:grab_focus()
		return false
	elseif ( vendor_exist(util:trim(ui.entrie_new_vendor.text)) ) then
		ui.entrie_vendor_message.label = 'La marca existe'
		ui.entrie_new_vendor:grab_focus()
		return false
	end

	local sql = [[insert into vendors (vendor) values(%s)]]
	local ok, err = db:execute(sql, util:trim(ui.entrie_new_vendor.text))
	if (not ok) then
		return false, err
	end
	local id_vendor = db:last_insert_rowid()
	populate_entries_vendor()
	ui.entrie_id_vendor:set_active_id(id_vendor)
	ui.entrie_new_vendor.text = ''
	return true
end

--- El boton añadir marca
function ui.entrie_add_vendor:on_clicked()
	ui.entrie_vendor_window:run()
	ui.entrie_vendor_window:hide()
end

--- El boton guardar marca
function ui.entrie_vendor_save:on_clicked()
	if ( save_vendor() ) then
		ui.entrie_vendor_window:hide()
	end
end

--- El boton cancelar guardar marca
function ui.entrie_vendor_cancel:on_clicked()
	ui.entrie_vendor_window:hide()
end