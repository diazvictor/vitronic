--[[--
 @package   Vitronic
 @filename  assistant.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      28.05.2020 23:01:53 -04
]]


local name, rif, address, phone, city


print( 'Iniciando el asistente de configuracion' )

---Valida la informacion del formulario
function validate_info()
	if(name and rif and address and phone and city)then
		assistant.property.page1.complete = true
	else
		if ( assistant.property.page1.complete ) then
			assistant.property.page1.complete = false
		end
	end
end

assistant = Gtk.Assistant {
	default_height = 300,
	window_position = 3,
	{
		title = "Empresa",
		type = 'INTRO',
		Gtk.Box {
			id = 'page1',
			orientation = 'VERTICAL',
			Gtk.Frame {
				label = "Nombre del Taller:",
				Gtk.Entry {
					id = 'name',
					margin_left = 5,
					margin_right = 5,
					margin_bottom = 5,
					vexpand = false,
					activates_default = false,
					on_changed = function()
						name = (  assistant.child.name.text ~= '' )
						validate_info()
					end					
				},
			},
			Gtk.Frame {
				label = "R.I.F:",
				Gtk.Entry {
					id = 'rif',
					margin_left = 5,
					margin_right = 5,
					margin_bottom = 5,
					vexpand = false,
					activates_default = false,
					on_changed = function()
						rif = (  assistant.child.rif.text ~= '' )
						validate_info()
					end	
				},
			},
			Gtk.Frame {
				label = "Dirección:",
				Gtk.Entry {
					id = 'address',
					margin_left = 5,
					margin_right = 5,
					margin_bottom = 5,
					vexpand = false,
					activates_default = false,
					on_changed = function()
						address = (  assistant.child.address.text ~= '' )
						validate_info()
					end	
				},
			},

			Gtk.Box {
				orientation = 'HORIZONTAL',
				Gtk.Frame {
					margin_right = 5,
					hexpand	    = true,
					label = "Telefono:",
					Gtk.Entry {
						id = 'phone',
						margin_left = 5,
						margin_right = 5,
						margin_bottom = 5,
						vexpand = false,
						activates_default = false,
						on_changed = function()
							phone = (  assistant.child.phone.text ~= '' )
							validate_info()
						end	
					},
				},
				Gtk.Frame {
					margin_left = 5,
					hexpand	    = true,
					label = "Ciudad:",
					Gtk.Entry {
						id = 'city',
						margin_left = 5,
						margin_right = 5,
						margin_bottom = 5,
						vexpand = false,
						activates_default = false,
						on_changed = function()
							city = (  assistant.child.city.text ~= '' )
							validate_info()
						end
					},
				},
			}
		}
	},
	{
		title = "Datos",
		complete = true,
		Gtk.Grid {
			orientation = 'VERTICAL',
			Gtk.CheckButton {
				id = 'examples',
				label = "Incluir datos de ejemplo"
			}
		}
	},
	{
		title = "Confirmación",
		type = 'CONFIRM',
		complete = true,
		Gtk.Label {
			label = "Confirma usted que los datos son correctos?"
		}
	},
	{
		title = "Aplicando los cambios",
		type  = 'PROGRESS',
		Gtk.Box {
			orientation = 'VERTICAL',
			Gtk.Label {
				label = "Espere mientras Vitronic se configura."
			},
			Gtk.ProgressBar {
				vexpand	    = true,
				id = 'progressbar',
				halign = 'CENTER',
				valign = 'CENTER'
			}
		}
	}
}


function assistant:on_cancel()
	self:destroy()
	Gtk.main_quit()
end

function assistant:on_close()
	self:destroy()
	Gtk.main_quit()
end

local progressbar = assistant.child.progressbar
function assistant:on_apply()
	local installing = false
	function setup()
		installing = true
		function save_company()
			local sql = [[
				insert into company (
					name,rif,address,phone,city
				) values(%s,%s,%s,%s,%s)
			]]
			local values = {
				assistant.child.name.text,
				assistant.child.rif.text,
				assistant.child.address.text,
				assistant.child.phone.text,
				assistant.child.city.text
			}
			local ok, err = db:execute(sql,values)
			if not ok then
				return false, err
			end
			return true
		end

		local file, err = io.open('schemas/schema.sql', 'rb')
		if err then return false, err end
		local db_schema = file:read('*a')
		file:close()
		db_dump 	= db:dump()
		local ok, err 	= db_dump:exec( db_schema  )
		if ( ok ) then
			if ( assistant.child.examples:get_active() ) then
				file, err = io.open('schemas/example.sql', 'rb')
				if not err then
					local examples = file:read('*a')
					file:close()
					ok, err = db_dump:exec(examples)
					if not ok then
						print('No se pudieron guardar los ejemplos', err)
					end
				end
			end
			save_company()
		end
	end

	GLib.timeout_add(GLib.PRIORITY_DEFAULT, 100,function()
		local fraction = progressbar.fraction + 0.01
		if (fraction < 1) then
			progressbar.fraction = fraction
			if( not installing ) then
				setup()
			end
			return true
		else
			assistant:destroy()
			require('components.splash')
			require('components.common')
			require('components.clients')
			require('components.entries')
			require('components.clock')
			require('components.settings')
			require('components.statusicon')
			--ui.main_window:show_all()
			return false
		end
	end)
end

assistant.child.page1:show_all()
assistant:show_all()
