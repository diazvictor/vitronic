--[[
 @package   Vitronic
 @filename  clock.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      26.05.2020 21:40:16 -04
]]--

ui.statusbar_datetime.label = os.date("%H:%M:%S")
GLib.timeout_add_seconds(
    GLib.PRIORITY_DEFAULT, 1,
    function()
        ui.statusbar_datetime.label = os.date("%H:%M:%S")
        return true
    end
)