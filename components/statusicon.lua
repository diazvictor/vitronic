--[[
 @package   Vitronic
 @filename  statusicon.lua
 @version   1.0
 @author    Díaz Urbaneja Víctor Eduardo Diex <victor.vector008@gmail.com>
 @author    Díaz Devera Víctor  aka  (Máster Vitronic) <vitronic2@gmail.com>
 @date      27.05.2020 15:35:37 -04
]]--

-- Crear el StatusIcon
statusicon = Gtk.StatusIcon({
	visible 	= false,
	tooltip_text 	= 'Vitronic',
	title 		= 'Workshop Management System'
})
-- Seteo la imagen del trayicon
statusicon:set_from_file('views/images/trayicon.png')
statusicon:set_name('Vitronic')


-- Crear un menu
function make_menu(event_button, event_time)
	-- @TODO: colocar iconos a los items
    local menu = Gtk.Menu {
        Gtk.ImageMenuItem {
            label = "Nuevo Cliente",
            image = Gtk.Image {
                stock = "gtk-new"
            },
            on_activate = function()
		ui.main_window:show_all()
                go_stack_form_client()
            end
        },
        Gtk.ImageMenuItem {
            label = "Nuevo Ingreso",
            image = Gtk.Image {
                stock = "gtk-new"
            },
            on_activate = function()
		ui.main_window:show_all()
                go_stack_form_entrie()
            end
        },
        Gtk.SeparatorMenuItem {},
        Gtk.ImageMenuItem {
            label = "Mostrar",
            image = Gtk.Image {
                stock = "gtk-missing-image"
            },
            on_activate = function()
                ui.main_window:show_all()
            end
        },
        Gtk.ImageMenuItem {
            label = "Ocultar",
            image = Gtk.Image {
                stock = "gtk-missing-image"
            },
            on_activate = function()
                ui.main_window:hide()
            end
        },
        Gtk.SeparatorMenuItem {},
        Gtk.ImageMenuItem {
            label = "Salir",
            image = Gtk.Image {
                stock = "gtk-quit"
            },
            on_activate = function()
                quit()
            end
        }
    }
    menu:show_all()
    menu:popup(nil, nil, nil, event_button, event_time)
end

---Mostrar ocultar ventana principal al hacer click al statusicon
local visible = false
function statusicon:on_activate()
	visible = not visible
	if ( visible ) then
		ui.main_window:show_all()
	else
		ui.main_window:hide()
	end
end

---EL menu del panel
function statusicon:on_popup_menu(ev, time)
	if (statusicon:get_visible() == true) then
		make_menu(ev, time)
	end
end

---Togle al statusicon
function statusicon_toggle()
	if (statusicon:get_visible() == false) then
		statusicon:set_visible(true)
	else
		statusicon:set_visible(false)
	end
end