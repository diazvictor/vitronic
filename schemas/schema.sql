/*
       _ _                   _      
__   _(_) |_ _ __ ___  _ __ (_) ___ 
\ \ / / | __| '__/ _ \| '_ \| |/ __|
 \ V /| | |_| | | (_) | | | | | (__ 
  \_/ |_|\__|_|  \___/|_| |_|_|\___|
*/

--soporte a claves foraneas
PRAGMA foreign_keys = on;

--La tabla de clientes
create table clients (
    id_client     integer primary key autoincrement,
    code          varchar,
    fullname      varchar,
    phone         varchar,
    other_phone   varchar,
    address       varchar,
    observation   varchar,
    passport_card varchar,
    created_at    datetime default (datetime('now','localtime')),
    unique(code),
    unique(passport_card),
    unique(phone)
);

--La tabla de marcas
create table vendors (
    id_vendor	integer primary key autoincrement,
    vendor	varchar,
    unique      (vendor)
);

--La tabla de entradas
create table entries (
    id_entrie	integer primary key autoincrement,   
    code        varchar not null,
    id_client	integer not null,
    id_vendor	integer not null,
    device	varchar not null,
    serial	varchar default null,
    problem	varchar not null,
    observation	varchar default null,
    delivery	date not null,
    quoted	decimal(12,2) default 0,
    created_at	datetime default (datetime('now','localtime')),
    foreign key	(id_client) references clients (id_client),
    foreign key	(id_vendor) references vendors (id_vendor)
);

---La tabla de anticipos sobre un ingreso
create table advances (
    id_advance	integer primary key autoincrement,
    id_entrie	integer not null,
    amount	decimal(12,2) not null,
    concept	varchar not null,
    created_at	datetime default (datetime('now','localtime')),
    foreign key	(id_entrie) references entries (id_entrie)
);

---La tabla de despachos de equipos
create table dispatchs (
    id_dispatch integer primary key autoincrement,   
    id_entrie	integer not null,
    amount_paid decimal(12,2) not null,
    observation	varchar default null,
    created_at	datetime default (datetime('now','localtime')),
    foreign key	(id_entrie) references entries (id_entrie)
);

---La tabla de presupuestos
create table budget (
    id_budget 	integer primary key autoincrement,
    code        varchar not null,
    id_client	integer not null,
    amount      decimal(12,2) not null,
    description varchar not null,
    observation	varchar default null,
    due_date	date not null,
    created_at	datetime default (datetime('now','localtime'))
);

---La tabla de la informacion de la empresa
create table company (
    id_company  integer primary key autoincrement,
    name        varchar,
    rif         varchar,
    address     varchar,
    phone       varchar,
    city        varchar
);

--- La tabla de configuraciones
create table settings (
    trayicon	boolean default true,
    language	varchar,
    date_format varchar
);
insert into
	settings(trayicon,language,date_format)
values ('t','es_ES.UTF-8','%d/%m/%Y')
