
---Datos de clientes
insert into clients (code,fullname,phone,other_phone,address,observation,passport_card) values 
('001','Jose Rodriguez','04142673894','02889947652','Upata','Dejo un televisor','31.888.547'),
('002','Eduardo Martinez','04128894672','02665436724','Upata','Dejo una radio','19.678.645'),
('003','Frank Gomez','04246523862','02665436723','Upata','Dejo una computadora','24.777.859'),
('004','Carlos Alvarez','04128907842','04248706534','Upata','Dejo un monitor','32.780.440'),
('005','Carlos Sanchez','0416987435','04167863794','Upata','Dejo un Raton','31.776.876'),
('006','Anthony Rodriguez','04267863794','02884438791','Upata','Dejo una laptop','32.780.886'),
('007','Carol Mejiaz','04247893456','04267789754','Upata','Dejo un cargador de canaimita','30.890.774'),
('008','Jhonatan Perez','04247896778','04268967547','Upata','Dejo un nintendo','31.776.456'),
('009','Daniel Herrera','04128904501','04128704902','Upata','Dejo un filtro','29.840.760'),
('010','Don Ramon','04267846394','04243658961','Upata','Dejo una nevera','26.896.224');

--Datos de marcas
insert into vendors (vendor) values
('SONY'),('PANASONY'),('HUAWEI'),
('CYBERLUX'),('LG'),('LENOVO'),
('OSTER'),('VIT'),('CHINO');

---Datos de ingresos
insert into entries (
	id_client,id_vendor,code,device,serial,problem, delivery)
values
(1,6,'0001','LAPTOP','575655','NO FURULA', '2020-05-28'),
(2,5,'0002','TV 20', '687866', 'NO ENCIENDE', '2020-05-31');