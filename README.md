[Vitronic](https://diazvictor.gitlab.io/vitronic/)
=====================

## Que es Vitronic

Vitronic es un sistema para gestion de talleres

![vitronic](views/images/screenshot.png)

## Instalacion

```
git clone https://gitlab.com/diazvictor/vitronic.git
cd vitronic/
make
[sudo] make check install
```

Probado con `lua5.1`, `luajit`, `lua5.2` y `lua5.3`

## dependencias

- [Lua](http://lua.org)
- [Lua-sqlite](http://lua.sqlite.org/index.cgi/home)
- [Lua-LGI](https://github.com/pavouk/lgi)