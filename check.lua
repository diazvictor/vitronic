#!/usr/bin/env lua

---Verifica la existencia de dependencias
function is_module(name)
	if package.loaded[name] then
		return true
	else
		for _, searcher in ipairs(package.searchers or package.loaders) do
			local loader = searcher(name)
			if type(loader) == 'function' then
				package.preload[name] = loader
				return true
			end
		end
		return false
	end
end

local depends = {
	'lsqlite3',
	'lgi'
}

for _,value in pairs(depends) do
	if ( not is_module(value) ) then
		print('Dependencia insatisfecha',value)
		os.exit(1)
	end
end
os.exit(0)
