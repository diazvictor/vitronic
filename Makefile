# Vitronic Makefile
#
# Copyright (c) 2020  Díaz  Urbaneja Victor Diego Alejandro  aka  (Sodomon)
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.

EXECUTABLE  = ./vitronic
LUA         = /usr/bin/lua5.1
MSGFMT      = /usr/bin/msgfmt
LOCALE_PO   = locale
LOCALES     = /usr/share/locale/es_ES.UTF-8/LC_MESSAGES

all: help

locale:
	$(MSGFMT) --check-accelerators=_ $(LOCALE_PO)/es.po -o $(LOCALE_PO)/es.mo

install: locale
	install -m775 $(EXECUTABLE) /usr/bin/
	mkdir -p /usr/share/vitronic
	install -m775 vitronic.lua /usr/share/vitronic
	cp -r components/ /usr/share/vitronic
	cp -r vendor/ /usr/share/vitronic
	cp -r libraries/ /usr/share/vitronic
	cp -r views/ /usr/share/vitronic
	cp -r schemas/ /usr/share/vitronic
	cp -r luachi.ini /usr/share/vitronic
	mkdir -p $(LOCALES)
	cp -r locale/es.mo $(LOCALES)/vitronic.mo
	cp -r views/images/logo.png /usr/share/pixmaps/vitronic.png
	install -m644 vitronic.desktop /usr/share/applications

uninstall :
	rm -r /usr/share/vitronic/
	rm -f /usr/share/applications/vitronic.desktop
	rm -f /usr/share/pixmaps/vitronic.png
	rm -f /usr/bin/$(EXECUTABLE)
	rm -f $(LOCALES)/vitronic.mo

check:
	$(LUA) check.lua

help:
	@echo
	@echo "Help"
	@echo "----"
	@echo
	@echo "  locale    - create translations"
	@echo "  install   - installs the program"
	@echo "  uninstall - uninstall the program"
	@echo "  check     - check that all dependencies are installed"
	@echo

.PHONY: locale check
